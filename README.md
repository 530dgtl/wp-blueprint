# WP-Blueprint

[Docker](https://www.docker.com) container that builds a [WordPress](https://hub.docker.com/_/wordpress), [MySQL](https://hub.docker.com/_/mysql) and [phpmyadmin](https://hub.docker.com/r/phpmyadmin/phpmyadmin) instance for local development.

## .gitignore

[.gitignore](.gitignore) is ignoring core [WordPress files](https://developer.wordpress.org/reference/) except for:

* [wp-content/](wp-content/) - Where your themes, plugins and uploads go
* [docker-compose.yaml](docker-compose.yaml) - File needed to the docker services
* [README.md](README.md) - Project README

The work that is done by development typically only resides in the [wp-content/](wp-content/) directory. There is no need for us to track core [WordPress files](https://developer.wordpress.org/reference/) in our revision control.

## What is it running?

* [mysql:5.7](https://hub.docker.com/_/mysql)
* [phpmyadmin/phpmyadmin](https://hub.docker.com/r/phpmyadmin/phpmyadmin)
* [wordpress:latest](https://hub.docker.com/_/wordpress)

## Setting versions

To adjust the version of [WordPress](https://hub.docker.com/_/wordpress), [MySQL](https://hub.docker.com/_/mysql) or [phpmyadmin](https://hub.docker.com/r/phpmyadmin/phpmyadmin) you are running simply change the [docker-compose.yaml](docker-compose.yaml) file to the versions you want to use.

## How to I use this for my project?

* Clone or Fork this repository as your starting point
* Update the [README.md](README.md) to reflect your project
* Adjust [docker-compose.yaml](docker-compose.yaml) values if needed
* Update these [package.json](package.json) values:
    * name
    * version - reset to 1.0.0
    * description
    * repository.url
    * author
    * license
    * homepage
* Run `docker-compose up` from the root directory
* View the site at [localhost:8000](localhost:8000)
* View phpmyadmin at [localhost:8080](localhost:8080)
* Connect with your mysql gui tool at [localhost:3306](localhost:3306)

## Credit

This is an modified version based on the original work by [Brad Traversy](https://gist.github.com/bradtraversy) in [this gist](https://gist.github.com/bradtraversy/faa8de544c62eef3f31de406982f1d42).
